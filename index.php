<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>KU Access Point Replacement - Contact</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/landing-page.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

<?php
    session_start();

    if (!(isset($_SESSION['logOn']) && $_SESSION['idTeacher'] != '')) {
        header ("Location: Login.php");
        exit();
    } else {
        $logOn =  $_SESSION['logOn'];
        $idTeacher =  $_SESSION['idTeacher'];
    }

    if ($logOn != "true") {
        header ("Location: Login.php");
        exit();
    }
?>

</head>
<style>
    button {
        font-size: 18px;
    }
</style>
<body>

    <div class="content-section-a">
        <div class="container">
            <div class="container" align="center">
            <div class="row">
                <div class="col-lg-12">
                    <table id="teacher_info" align="center" style="border-style: solid;">
                        <tr>
                            <td>รหัสประจำตัว</td>
                            <td id="teacher_id"><?php
                                $idTeacher = $_SESSION['idTeacher'];
                                echo str_replace('"', '', $idTeacher);
                                // echo $idTeacher;
                            ?></td>
                        </tr>
                    </table>
                </div>
            </div>
            <button type="button" class="btn-primary" onclick="goToClass();">Class Teacher</button>
            <button type="button" class="btn-primary" onclick="goToSubject();">Subject Teacher</button>
        </div>
    </div>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <script src="js/custom_script.js "></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.js"></script>

    <script>
        $(document).ready(function() {
        alert("ยินดีต้อนรับสู่ระบบ");
            $.ajax({
                type: "POST",
                url: "controller/php_function_switcher.php",
                data: {
                    func: 'get_login',
                    username: $('#teacher_id').html()
                },
                success: function(data) {                    
                    var obj = JSON.parse(data);                    
                    if (obj == "") {
                        alert("Cannot find username in system");
                    } else {
                        var outData = '<table id="teacher_info" align="center" style="border-style: solid;">';
                        outData += '<tr><td>รหัสประจำตัว</td>';
                        outData += '<td>'+obj['id']+'</td></tr>';
                        outData += '<tr><td>ชื่อ - นามสกุล</td>';
                        outData += '<td>'+obj['firstname']+" "+obj['lastname']+'</td></tr>';
                        outData += '<tr><td>level</td>';
                        outData += '<td>'+obj['level']+'</td></tr>';
                        outData += '</table>';
                        $('#teacher_info').html(outData);
                    }
                },
                error: function(data) {
                    alert("error " + data);
                }
            });
        });
    </script>

</body>

</html>
