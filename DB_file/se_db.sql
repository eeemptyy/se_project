-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 08, 2016 at 08:29 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `se_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `classteacher`
--

CREATE TABLE `classteacher` (
  `id_teacher` varchar(16) NOT NULL,
  `id_student` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `classteacher`
--

INSERT INTO `classteacher` (`id_teacher`, `id_student`) VALUES
('5610450063', '257 00 81007'),
('5610450063', '285 00 81060'),
('5610450080', '256 00 81050'),
('5610450080', '257 00 81101'),
('5610450926', '255 00 81111');

-- --------------------------------------------------------

--
-- Table structure for table `confirm_grade`
--

CREATE TABLE `confirm_grade` (
  `id_teacher` varchar(16) NOT NULL,
  `clickable` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `confirm_grade`
--

INSERT INTO `confirm_grade` (`id_teacher`, `clickable`) VALUES
('5610450063', 1),
('5610450080', 1),
('5610450250', 1),
('5610450926', 1),
('5710450032', 1),
('5710450332', 1),
('5710450999', 1),
('5710451151', 1);

-- --------------------------------------------------------

--
-- Table structure for table `deadline_grade`
--

CREATE TABLE `deadline_grade` (
  `id` int(11) NOT NULL,
  `datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE `program` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`id`, `name`) VALUES
(1, 'special'),
(2, 'normal');

-- --------------------------------------------------------

--
-- Table structure for table `semester`
--

CREATE TABLE `semester` (
  `id_subject` varchar(16) NOT NULL,
  `semester` int(1) NOT NULL,
  `year` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `semester`
--

INSERT INTO `semester` (`id_subject`, `semester`, `year`) VALUES
('ART_32140', 1, 2016),
('ENG_30288', 1, 2016),
('ENG_32103', 1, 2016),
('HPE_32123', 1, 2016),
('MATH_32113', 1, 2016),
('SCI_32275', 1, 2016),
('SOC_32103', 1, 2016),
('THA_32103', 1, 2016),
('WORK_32108', 1, 2016);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` varchar(16) NOT NULL,
  `id_program` int(11) NOT NULL,
  `firstname` varchar(128) NOT NULL,
  `lastname` varchar(128) NOT NULL,
  `degree` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `id_program`, `firstname`, `lastname`, `degree`) VALUES
('255 00 81111', 1, 'Phanuvich', 'Hirunsirisombut', 2),
('256 00 81050', 1, 'Chayamon ', 'Kanchanapongsavet', 3),
('257 00 81007', 2, 'Amonrada', 'Krityapichartkul', 2),
('257 00 81101', 2, 'Jompol', 'Sermsuk', 3),
('285 00 81060', 1, 'Amonrada', 'Yodpinit', 1);

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE `subject` (
  `id` varchar(16) NOT NULL,
  `name` varchar(128) NOT NULL,
  `credit` int(1) NOT NULL,
  `creditPerHr` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`id`, `name`, `credit`, `creditPerHr`) VALUES
('ART_32140', 'Art 3', 1, 1),
('ENG_30288', 'Listening for Academic Purpose', 1, 3),
('ENG_32103', 'English 3', 3, 5),
('HPE_32123', 'Physical Education 3', 1, 2),
('MATH_32113', 'Mathematics 3', 3, 4),
('SCI_32275', 'Physics 2', 2, 3),
('SOC_32103', 'Social Studies and Culture 3', 1, 3),
('THA_32103', 'Thai Language 3', 2, 5),
('WORK_32108', 'Personal Finance ', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `subjectteacher`
--

CREATE TABLE `subjectteacher` (
  `id_teacher` varchar(16) NOT NULL,
  `id_subject` varchar(16) NOT NULL,
  `semester` int(1) NOT NULL,
  `year` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subjectteacher`
--

INSERT INTO `subjectteacher` (`id_teacher`, `id_subject`, `semester`, `year`) VALUES
('5610450063', 'ART_32140', 1, 2016),
('5610450063', 'THA_32103', 1, 2016),
('5610450080', 'MATH_32113', 1, 2016),
('5610450926', 'SOC_32103', 1, 2016),
('5710450032', 'SCI_32275', 1, 2016),
('5710450332', 'ENG_30288', 1, 2016),
('5710450332', 'ENG_32103', 1, 2016),
('5710450999', 'WORK_32108', 1, 2016),
('5710451151', 'HPE_32123', 1, 2016);

-- --------------------------------------------------------

--
-- Table structure for table `takes`
--

CREATE TABLE `takes` (
  `id_student` varchar(16) NOT NULL,
  `id_subject` varchar(16) NOT NULL,
  `semester` int(1) NOT NULL,
  `year` int(4) NOT NULL,
  `quarter1` int(3) NOT NULL,
  `quarter2` int(3) NOT NULL,
  `quarter3` int(3) NOT NULL,
  `grade` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `takes`
--

INSERT INTO `takes` (`id_student`, `id_subject`, `semester`, `year`, `quarter1`, `quarter2`, `quarter3`, `grade`) VALUES
('255 00 81111', 'ENG_30288', 1, 2016, 0, 0, 0, 0),
('256 00 81050', 'MATH_32113', 1, 2016, 0, 0, 0, 0),
('256 00 81050', 'WORK_32108', 1, 2016, 0, 0, 0, 0),
('257 00 81007', 'SCI_32275', 1, 2016, 0, 0, 0, 0),
('257 00 81007', 'SOC_32103', 1, 2016, 0, 0, 0, 0),
('257 00 81101', 'ENG_32103', 1, 2016, 0, 0, 0, 0),
('257 00 81101', 'HPE_32123', 1, 2016, 0, 0, 0, 0),
('285 00 81060', 'ART_32140', 1, 2016, 0, 0, 0, 0),
('285 00 81060', 'THA_32103', 1, 2016, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--

CREATE TABLE `teacher` (
  `id` varchar(16) NOT NULL,
  `firstname` varchar(128) NOT NULL,
  `lastname` varchar(128) NOT NULL,
  `level` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `teacher`
--

INSERT INTO `teacher` (`id`, `firstname`, `lastname`, `level`) VALUES
('5610450063', 'Somchai', 'Jaidee', 1),
('5610450080', 'Somying', 'Deejai', 1),
('5610450250', 'Pongsakorn', 'Ruedeerat', 2),
('5610450926', 'Janevit', 'Dechnumbunchachai', 1),
('5710450032', 'Vitaya', 'Pongpai', 2),
('5710450332', 'Kondee', 'Sritium', 2),
('5710450999', 'Somkuan', 'Suksai', 3),
('5710451151', 'Chokchai', 'Hirunpueak', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `classteacher`
--
ALTER TABLE `classteacher`
  ADD PRIMARY KEY (`id_teacher`,`id_student`);

--
-- Indexes for table `confirm_grade`
--
ALTER TABLE `confirm_grade`
  ADD PRIMARY KEY (`id_teacher`);

--
-- Indexes for table `deadline_grade`
--
ALTER TABLE `deadline_grade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes f