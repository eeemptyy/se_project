<!DOCTYPE html>
<html>

<head>
    <title>ยินดีต้อนรับสู่ระบบออกเกรดสมุดพกนักเรียน โรงเรียนสาธิตแห่งมหาวิทยาลัยเกษตรศาสตร์</title>
    <style>
        body {
            background-image: url("img/background-purple-flower.gif")
        }
        
        img {
            display: block;
            margin: auto;
        }
        
        h1 {
            text-align: center;
            font-size: 20px;
        }
        
        h2 {
            text-align: center;
            font-size: 30px;
        }
        
        .div2 {
            margin: auto;
            width: 300px;
            height: 130px;
            padding: 25px;
            border: 2px solid black;
        }
        
        .div3 {
            text-align: center;
            font-size: 18px;
        }
        
        .login-block {
            width: 320px;
            padding: 20px;
            border-radius: 5px;
            margin: 0 auto;
        }
        
        .login-block h1 {
            text-align: center;
            color: #000;
            font-size: 18px;
            text-transform: uppercase;
            margin-top: 0;
            margin-bottom: 20px;
        }
        
        .login-block input {
            width: 100%;
            height: 42px;
            box-sizing: border-box;
            border-radius: 5px;
            border: 1px soli black;
            margin-bottom: 20px;
            font-size: 14px;
            font-family: Montserrat;
            padding: 0 20px 0 50px;
            outline: none;
        }
        
        .login-block input#username {
            background: lightpink url('http://i.imgur.com/u0XmBmv.png') 20px top no-repeat;
            background-size: 16px 80px;
        }
        
        .login-block input#username:focus {
            background: lightyellow url('http://i.imgur.com/u0XmBmv.png') 20px bottom no-repeat;
            background-size: 16px 80px;
        }
        
        .login-block input#password {
            background: lightpink url('http://i.imgur.com/Qf83FTt.png') 20px top no-repeat;
            background-size: 16px 80px;
        }
        
        .login-block input#password:focus {
            background: lightyellow url('http://i.imgur.com/Qf83FTt.png') 20px bottom no-repeat;
            background-size: 16px 80px;
        }
        
        .login-block button {
            width: 100%;
            height: 40px;
            background: lightsalmon;
            box-sizing: border-box;
            border-radius: 5px;
            border: 1px solid rebeccapurple;
            color: black;
            font-weight: bold;
            text-transform: uppercase;
            font-size: 14px;
            font-family: Montserrat;
            outline: none;
            cursor: pointer;
        }
        
        label.checkbox-inline1 {
            margin-left: 510px;
            ;
            font-size: 20px;
            text-align: center;
        }
        
        label.checkbox-inline2 {
            margin-left: 40px;
            ;
            font-size: 20px;
            text-align: center;
        }
    </style>
</head>

<body>
    <img src="img/ooo.gif" ait="Pirun" style="width:17% height=20%">
    <h1>ยินดีต้อนรับสู่</h1>
    <h2>ระบบออกเกรดสมุดพกนักเรียน</h2>

    <div class="logo"></div>
    <div class="login-block">
        <input type="text" value="" placeholder="Username" id="username" />
        <input type="password" value="" placeholder="Password" id="password" />
        <button type="button" onclick="confirmInput();">Submit</button>
    </div>

    <p id="output">

    </p>
</body>

<script src="https://code.jquery.com/jquery-3.1.1.js" integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA=" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.1.1.js" integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA=" crossorigin="anonymous"></script>
<script>
    function isEmpty(data) {
        if (data.trim() == "") {
            return true;
        }
        return false;
    }

    function confirmInput() {
        var uname = $('#username').val();
        var pass = $('#password').val();
        if (isEmpty(uname) || isEmpty(pass) || pass == "Password" || uname == "Useername") {
            alert("Invalid input.");
        } else {            
            ss(uname, pass);
        }
    }

    function ss(uname, pass) {
        $.ajax({
            type: "POST",
            url: "controller/php_function_switcher.php",
            data: {
                func: 'get_login',
                username: uname
            },
            success: function(data) {                
                var obj = JSON.parse(data);                
                if (obj == "") {
                    alert("Cannot find username in system");
                } else {
                    // window.location = 'controller/create_session.php?username=' + obj['id'];
                    // window.location.href = "http://stackoverflow.com";
                    if (obj['password'] == pass){
                        window.location.href = 'controller/create_session.php?username="' + obj['id'] + '"';
                    }else {
                        alert("Incorrect password.");
                    }
                }
            },
            error: function(data) {
                alert("error " + data);
            }
        });

    }
</script>

</html>