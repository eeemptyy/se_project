<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Landing Page - Start Bootstrap Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/landing-page.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<?php
    session_start();

    if (!(isset($_SESSION['logOn']) && $_SESSION['idTeacher'] != '')) {
        header ("Location: Login.php");
        exit();
    } else {
        $logOn =  $_SESSION['logOn'];
        $idTeacher =  $_SESSION['idTeacher'];
    }

    if ($logOn != "true") {
        header ("Location: Login.php");
        exit();
    }
?>
</head>

<style>
    button.logoutBtn {
        background-color: red;
        text-align: center;
        font-size: 18px;
    }
    
    table,
    th,
    td {
        border: 2px solid black;
        text-align: center;
    }
    
    table {
        border-collapse: collapse;
        width: 80%;
    }
    
    h2,
    h3 {
        text-align: center;
        font-size: 20px;
        text-shadow: 2px 2px 3px rgba(0, 0, 0, 0.6);
    }
</style>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top topnav" role="navigation">
        <div class="container topnav">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <!-- <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button> -->

                <a class="navbar-brand topnav" href="#">

                    </select> Teacher ID : <input type="text"  id="id_teacher">
                    <!--    <br> Teacher ID : <input type="text" value="5710450332" id="id_teacher"> </br> -->

                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" size="15px" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <button class="logoutBtn" onclick="logOut();">Log out</button>
                    </li>
                </ul>

            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <a name="contact"></a>
    <div class="content-section-a">
        <h2 id="studentName"></h2>
        <h3 id="studentId"></h3>

        <!-- /.container -->


        <table id="studentClass" align="center" style="border-style: solid;">
            <tr>
                <th>No.</th>
                <th>Student ID</th>
                <th>firstname</th>
                <th>lastname</th>
                
            </tr>
        </table>

        <table id="studentCourse" align="center" style="display:none" style="border-style: solid;">
            <tr>
                <th>No.</th>
                <th>Subject ID</th>
                <th>Course Name</th>
                <th>Credit</th>
                <th>Q1</th>
                <th>Q2</th>
                <th>Q3</th>
                <th>Semester Grade</th>
                <th></th>
                <th></th>
            </tr>
        </table>



    </div>
    </div>

    <div class="content-section-b " align="center ">
        <button class="gobtn btn-success" type="button" id="go" onclick="getStudentInClass('id_teacher')">GO</button>

    </div>
    <!-- /.banner -->

    <!--  <footer>
        <div class="container ">
            <div class="row ">
                <div class="col-lg-12 ">
                    <ul class="list-inline ">
                        <li>
                            <a href="# ">Home</a>
                        </li>
                        <li class="footer-menu-divider ">&sdot;</li>
                        <li>
                            <a href="#about ">About</a>
                        </li>
                        <li class="footer-menu-divider ">&sdot;</li>
                        <li>
                            <a href="#services ">Services</a>
                        </li>
                        <li class="footer-menu-divider ">&sdot;</li>
                        <li>
                            <a href="#contact ">Contact</a>
                        </li>
                    </ul>
                    <p class="copyright text-muted small ">Copyright &copy; Your Company 2014. All Rights Reserved</p>
                </div>
            </div>
        </div>
    </footer> -->

    <!-- jQuery -->
    <script src="js/jquery.js "></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="https://code.jquery.com/jquery-3.1.1.js" integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA=" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="js/custom_script.js"></script>
</body>

</html>