<?php
   require 'db_controller.php';
   require 'generate_report.php';

    /* this file will act as a gate to db_controller on ajax called */ 

    $db_controller = new DB_Controller();

    $case_sw = $_POST['func'];
   
    switch ($case_sw) {
        case "get_data":
            echo $db_controller->getData();
            break;
        case "get_login":
            echo $db_controller->getLogin($_POST['username']);
            break;
        case "get_coursename":
            echo $db_controller->getCoursename($_POST['year'], $_POST['semester'], $_POST['idTeacher']);
            break;
        case "get_student_in_course":
            echo $db_controller->getStudentInCourse($_POST['idSubject'], $_POST['year'], $_POST['semester'], $_POST['idTeacher']);
            break;
        case "get_student_in_class":
            echo $db_controller->getStudentInClass($_POST['idTeacher']);
            break;
        case "get_course_in_student":
            echo $db_controller->getCourseInStudent($_POST['idStudent']);
            break;
        case "update_quarter1":
            echo $db_controller->updateQuarter1($_POST['students']);
            break;
        case "update_quarter2":
            echo $db_controller->updateQuarter2($_POST['students']);
            break;
        case "check_confirm_button":
            echo $db_controller->getButtonClickable($_POST['idTeacher']);
            break;
        case "gen_report":
            $generator = new GenerateReport($db_controller, $_POST['student_id']);
            $generator->genHeader();
            $generator->genFirstTable();
            $generator->genSecondTable();
            echo $generator->getReport();
            break;  
        default:
            echo "Function Not Found.";
}

?>