<?php

class DB_Controller
{
    
    private $username = "";
    private $password = "";
    private $hostname = "";
    private $dbname = "";

    private $connection;
    private $nowSemester = "";
    public $nowYear = "";
    
    public function __construct()
    {
        include("dbconfig.php");
        $this->username = $uname;
        $this->password = $pass;
        $this->hostname = $host;
        $this->dbname = $db;
        
        try {
            $this->connection = new PDO("mysql:host={$this->hostname};"."dbname={$this->dbname}", $this->username, $this->password);
            // $this->connection->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            die("Couldn't connect to the database ".$dbname.": ".$e->getMessage());
        }

        $this->getNowData();
    }

    private function getNowData()
    {
        try {
            $sql = 'SELECT semester, year FROM semester ORDER BY year DESC , semester DESC LIMIT 1';
            $q = $this->connection->query($sql);
            $q->setFetchMode(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            die($e->getMessage());
        }
        while ($row = $q->fetch()) {
            $this->nowSemester = $row['semester'];
            $this->nowYear = $row['year'];
        }
    }

    public function getLogin($id_teacher)
    {
        try {
            $sql = 'select * from teacher where id = "'.$id_teacher.'"';
            $q = $this->connection->query($sql);
            $q->setFetchMode(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            die($e->getMessage());
        }
        
        $teacher=array();
        while ($row = $q->fetch()) {
            $teacher['id'] = $row['id'];
            $teacher['firstname'] = $row['firstname'];
            $teacher['lastname'] = $row['lastname'];
            $teacher['level'] = $row['level'];
            $teacher['password'] = $row['password'];
        }
        return json_encode($teacher);
    }

    public function getData()
    {
        $students=array();
        try {
            $sql = 'select * from Student';
            $q = $this->connection->query($sql);
            $q->setFetchMode(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            die($e->getMessage());
        }
        $out = "";
        $n = 0;
        while ($row = $q->fetch()) {
            $student=array();
            $student['id'] = $row['id'];
            $student['id_program'] = $row['id_program'];
            $student['firstname'] = $row['firstname'];
            $student['lastname'] = $row['lastname'];
            $students[$n] = $student;
            $n++;
        }
        return json_encode($students);
    }

    public function getCoursename($year, $semester, $idTeacher)
    {
        $course=array();
        try {
            $sql = 'SELECT subjectteacher.id_teacher, subjectteacher.id_subject, subject.name, subject.credit, subject.creditPerHr, subjectteacher.semester, subjectteacher.year'.
                    ' FROM subjectteacher, subject'.
                    ' WHERE subjectteacher.id_teacher = "'.$idTeacher.'" AND subject.id = subjectteacher.id_subject AND subjectteacher.semester = "'.$semester.'" AND subjectteacher.year = "'.$year.'"'.
                    ' GROUP BY subjectteacher.id_subject';
            $q = $this->connection->query($sql);
            $q->setFetchMode(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            die($e->getMessage());
        }
        $n=0;
        while ($row = $q->fetch()) {
            $subject=array();
            $subject['id_teacher'] = $row['id_teacher'];
            $subject['id_subject'] = $row['id_subject'];
            $subject['name'] = $row['name'];
            $subject['credit'] = $row['credit'];
            $subject['creditPerHr'] = $row['creditPerHr'];
            $subject['semester'] = $row['semester'];
            $subject['year'] = $row['year'];
            $course[$n] = $subject;
            $n++;
        }
        return json_encode($course);
    }

    public function getStudentInCourse($idSubject, $year, $semester, $idTeacher)
    {
        $students=array();
        try {
            $sql = 'SELECT takes.id_subject AS subject_id, subject.name AS subject_name, takes.id_student AS student_id, student.firstname, student.lastname, student.id_program AS program, takes.quarter1, takes.quarter2, takes.quarter3, takes.grade, student.degree'.
                    ' FROM takes, student, subject'.
                    ' WHERE takes.id_subject = "'.$idSubject.'" AND takes.semester = "'.$semester.'" AND takes.year = "'.$year.'" AND takes.id_student = student.id AND subject.id = takes.id_subject';
            $q = $this->connection->query($sql);
            $q->setFetchMode(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            die($e->getMessage());
        }
        $n=0;
        while ($row = $q->fetch()) {
            $student=array();
            $student['subject_id'] = $row['subject_id'];
            $student['subject_name'] = $row['subject_name'];
            $student['student_id'] = $row['student_id'];
            $student['firstname'] = $row['firstname'];
            $student['lastname'] = $row['lastname'];
            $student['program'] = $row['program'];
            $student['quarter1'] = $row['quarter1'];
            $student['quarter2'] = $row['quarter2'];
            $student['quarter3'] = $row['quarter3'];
            $student['grade'] = $row['grade'];
            $student['degree'] = $row['degree'];
            $students[$n] = $student;
            $n++;
        }
        return json_encode($students);
    }

    public function getStudentInClass($idTeacher)
    {
        $students=array();
        try {
            $sql = 'SELECT classteacher.id_teacher AS teacher_id, classteacher.id_student AS student_id, student.firstname, student.lastname, student.id_program AS program, student.degree'.
                    ' FROM classteacher, student'.
                    ' WHERE id_teacher = "'.$idTeacher.'" AND student.id = classteacher.id_student';
            $q = $this->connection->query($sql);
            $q->setFetchMode(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            die($e->getMessage());
        }
        $n=0;
        while ($row = $q->fetch()) {
            $student=array();
            $student['teacher_id'] = $row['teacher_id'];
            $student['student_id'] = $row['student_id'];
            $student['firstname'] = $row['firstname'];
            $student['lastname'] = $row['lastname'];
            $student['program'] = $row['program'];
            $student['degree'] = $row['degree'];
            $students[$n] = $student;
            $n++;
        }
        return json_encode($students);
    }

    public function getCourseInStudent($idStudent)
    {
        $subjects = array();
        try {
            $sql = 'SELECT takes.id_subject AS subject_id, subject.name AS subject_name, subject.credit, takes.quarter1, takes.quarter2, takes.quarter3, takes.grade'.
                    ' FROM takes, subject'.
                    ' WHERE takes.id_student = "'.$idStudent.'" AND semester = "'.$this->nowSemester.'" AND year = "'.$this->nowYear.'" AND subject.id = takes.id_subject';
            $q = $this->connection->query($sql);
            $q->setFetchMode(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            die($e->getMessage());
        }
        $n = 0;
        while ($row = $q->fetch()) {
            $subject=array();
            $subject['subject_id'] = $row['subject_id'];
            $subject['subject_name'] = $row['subject_name'];
            $subject['credit'] = $row['credit'];
            $subject['quarter1'] = $row['quarter1'];
            $subject['quarter2'] = $row['quarter2'];
            $subject['quarter3'] = $row['quarter3'];
            $subject['grade'] = $row['grade'];
            $subjects[$n] = $subject;
            $n++;
        }
        return json_encode($subjects);
    }

    public function updateQuarter1($json_students)
    {
        $students = json_decode($json_students);
        $students_dict = array();
        foreach ($students as $key => $val) {
            $students_dict[$key] = $val;
        }
        $idSubject = $students_dict['subject_id'];
        $idStudent = $students_dict['student_id'];
        $quarter1 = $students_dict['quarter1'];
        $quarter2 = $students_dict['quarter2'];
        $quarter3 = $students_dict['quarter3'];
        $year = $students_dict['year'];
        $semester = $students_dict['semester'];
        $grade = $students_dict['grade'];
        try {
            $sql = 'UPDATE takes'.
                    ' SET quarter1 = "'.$quarter1.'", quarter2 = "'.$quarter2.'", quarter3 = "'.$quarter3.'", grade = "'.$grade.'"'.
                    ' WHERE id_student = "'.$idStudent.'" AND id_subject = "'.$idSubject.'" AND semester = "'.$semester.'" AND year = "'.$year.'"';
            $q = $this->connection->prepare($sql);
            $q->execute();
        } catch (Exception $e) {
            die($e->getMessage());
        }
            // return "Server Response. $idSubject $idStudent $quarter1 $quarter2 $quarter3 $semester $year";
            return "Server Response.";
    }

    public function updateQuarter2($json_students)
    {
        $students = json_decode($json_students);
        $students_dict = array();
        foreach ($students as $key => $val) {
            $students_dict[$key] = $val;
        }
        $idSubject = $students_dict['subject_id'];
        $idStudent = $students_dict['student_id'];
        $quarter1 = $students_dict['quarter1'];
        $quarter2 = $students_dict['quarter2'];
        $quarter3 = $students_dict['quarter3'];
        $grade = $students_dict['grade'];
        try {
            $sql = 'UPDATE takes'.
                    ' SET quarter1 = "'.$quarter1.'", quarter2 = "'.$quarter2.'", quarter3 = "'.$quarter3.'", grade = "'.$grade.'"'.
                    ' WHERE id_student = "'.$idStudent.'" AND id_subject = "'.$idSubject.'" AND semester = "'.$this->nowSemester.'" AND year = "'.$this->nowYear.'"';
            $q = $this->connection->prepare($sql);
            $q->execute();
        } catch (Exception $e) {
            die($e->getMessage());
        }
        // return "Server Response. $idSubject $idStudent $quarter1 $quarter2 $quarter3 $this->nowSemester $this->nowYear";
        return "Server Response.";
    }

    public function getButtonClickable($idTeacher)
    {
        try {
            $sql = 'SELECT *'.
                    ' FROM confirm_grade'.
                    ' WHERE confirm_grade.id_teacher = "'.$idTeacher.'"';
            $q = $this->connection->query($sql);
            $q->setFetchMode(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            die($e->getMessage());
        }
        while ($row = $q->fetch()) {
            $c = $row['clickable'];
            break;
        }
        return $c;
    }


/////////////////////////////// > Data function for generate Report < ///////////////////////////////

    public function getStudentData($idStudent)
    {
        try {
            $sql = 'SELECT *'.
                    ' FROM student'.
                    ' WHERE id = "'.$idStudent.'"';
            $q = $this->connection->query($sql);
            $q->setFetchMode(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            die($e->getMessage());
        }
        $student = array();
        while ($row = $q->fetch()) {
            $student['student_id'] = $row['id'];
            $student['student_name'] = $row['firstname']." ".$row['lastname'];
            $student['student_degree'] = $row['degree'];
            $student['student_program'] = $row['id_program'];
            break;
        }
        return $student;
    }

    public function getTeacherOfStudent($idStudent)
    {
        try {
            $sql = 'SELECT classteacher.id_teacher as teacher_id, teacher.firstname, teacher.lastname'.
                    ' FROM classteacher, teacher'.
                    ' WHERE classteacher.id_student = "'.$idStudent.'" AND teacher.id = classteacher.id_teacher';
            $q = $this->connection->query($sql);
            $q->setFetchMode(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            die($e->getMessage());
        }
        $teachers = array();
        $n=0;
        while ($row = $q->fetch()) {
            $teacher = array();
            $teacher['teacher_id'] = $row['teacher_id'];
            $teacher['teacher_name'] = $row['firstname']." ".$row['lastname'];
            $teachers[$n] = $teacher;
            $n++;
        }
        return $teachers;
    }

    public function getAllCourse($idStudent)
    {
        $this->getNowData();
        try {
            $sql = 'SELECT takes.id_subject AS subject_id'.
                    ' FROM takes'.
                    ' WHERE takes.id_student = "'.$idStudent.'" AND takes.year = "'.$this->nowYear.'"';
            $q = $this->connection->query($sql);
            $q->setFetchMode(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            die($e->getMessage());
        }
        $n=0;
        $courses = array();
        while ($row = $q->fetch()) {
            $courses[$n] = $row['subject_id'];
            $n++;
        }
        return $courses;
    }

    public function getCourseDetails($idSubject){
         try {
            $sql = 'SELECT subject.name, takes.id_subject AS subject_id, subject.credit, takes.quarter1, takes.quarter2, takes.quarter3, takes.grade'.
                    ' FROM takes, subject'. 
                    ' WHERE takes.id_subject = "'.$idSubject.'" AND takes.year = "'.$this->nowYear.'" AND subject.id = takes.id_subject';
            $q = $this->connection->query($sql);
            $q->setFetchMode(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            die($e->getMessage());
        }
        $courses = array();
        while ($row = $q->fetch()) {
            $courses['name'] = $row['name'];
            $courses['subject_id'] = $row['subject_id'];
            $courses['credit'] = $row['credit'];
            $courses['quarter1'] = $row['quarter1'];
            $courses['quarter2'] = $row['quarter2'];
            $courses['quarter3'] = $row['quarter3'];
            $courses['grade'] = $row['grade'];
            break;
        }
        return $courses;
    }

}
