<?php

// require 'db_controller.php';

class GenerateReport
{
    private $report = '<div id="report">';
    private $db_controller = "";
    private $student = "";
    private $teachers = "";
    private $allCourse = "";

    public function __construct($db_controller, $student_id)
    {
        $this->db_controller = $db_controller;
        $this->getStudentData($student_id);

        $this->getStudentData($student_id);
        $this->getTeacherOfStudent($student_id);
        $this->getAllCourse($student_id);
    }

    public function getStudentData($student_id){
        $this->student = $this->db_controller->getStudentData($student_id);
    }
    public function getTeacherOfStudent($student_id){
        $this->teachers = $this->db_controller->getTeacherOfStudent($student_id);
    }
    public function getAllCourse($student_id){
        $this->allCourse = $this->db_controller->getAllCourse($student_id);
    }

    public function genHeader(){
        $this->report .= '<table class="mytable" style="border: hidden;">
        <thead class="noBorder">
                 <tr>
                  <th colspan="15" style="text-align:left;">Grade '.$this->student['student_degree'].' : '.$this->db_controller->nowYear.'</th>
                  <th colspan="6" style="text-align:left;">Classroom Teacher</th>
                  </tr>
                <tr>';
            // $teacher['teacher_id'] = $row['teacher_id'];
            // $teacher['teacher_name'] = $row['firstname']." ".$row['lastname'];
        // $x = sizeof($this->teachers);
        // while ($x < 3){
        //     $teacher = array();
        //     $teacher['teacher_id'] = " ";
        //     $teacher['teacher_name'] = " ";
        //     $this->teachers[$x] = $teacher;
        //     $x = sizeof($this->teachers);
        // }
                
        $this->report .= '<th colspan="15" style="text-align:left;">Student Name : '.$this->student['student_name'].'</th>
                        <td colspan="6" style="text-align:left;">'.$this->teachers[0]['teacher_name'].'</td>
                </tr>
                <tr>
                    <th colspan="15" style="text-align:left;">Student ID : '.$this->student['student_id'].'</th>
                   <!--<th colspan="10">&nbsp;</th>-->
                    <td colspan="6" style="text-align:left;">&nbsp;</td>
                </tr>
                <tr>
                    <th colspan="15" style="text-align:left;">&nbsp;</th>
                    <td colspan="6" style="text-align:left;">&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </thead>';

    }

    public function genFirstTable(){
        $this->report .= '<thead>
                <tr>
                    <th rowspan="2" colspan="6">Course</th>
                    <th rowspan="2" colspan="2">Code</th>
                    <th colspan="2">Periods/Week;</th>
                    <th rowspan="2">Credit<br>Hour</th>
                    <th colspan="4" colspan="4">1st Semester Grade</th>
                    <th colspan="4" colspan="4">2st Semester Grade</th>
                    <th rowspan="2" colspan="2">Year<br>Grade</th>
                </tr>
                <tr>
                    <th>In Class</th>
                    <th>Practice</th>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>Sem.<br>Grade</th>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>Sem.<br>Grade</th>
                </tr>
            </thead>
            <tbody>';
            $sumCredit = 0;
            foreach ($this->allCourse as $id) {
                $subject = $this->db_controller->getCourseDetails($id);
                $this->report .= '<tr>
                <td colspan="6">'.$subject['name'].' '.sizeof($this->allCourse).'xx'.'</td>
                <td colspan="2">'.$subject['subject_id'].'</td>
                <td>'.$subject['credit'].'</td>
                <td>-</td>
                <td>'.$subject['credit'].'.00</td>
                <td>'.$subject['quarter1'].'</td>
                <td>'.$subject['quarter2'].'</td>
                <td>'.$subject['quarter3'].'</td>
                <td>-</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>-</td>
                    <td colspan="2">-</td>
                    </tr>';
                $sumCredit += $subject['credit'];
            }

            $this->report .= '
                <tr>
                    <th colspan="10">Total</th>
                    <th>'.$sumCredit.'.00</th>
                    <td colspan="3">1st Semester Grade</td>
                    <td>&nbsp;</td>
                    <td colspan="3">2st Semester Grade</td>
                    <td>&nbsp;</td>
                    <th colspan="2">&nbsp;</th>
                </tr>
                <tr class="noBorder">
                    <td colspan="21"> &nbsp;</td>
                </tr>';
                 $this->report .= '
                <tr>
                    <th colspan="6">Activity (S/U)</th>
                    <th colspan="2">1st Sem</th>
                    <th colspan="2">2st Sem</th>
                    <th class="noBorder" colspan="4">&nbsp;</th>
                    <th colspan="7">Cumulative GPA/'.$sumCredit.'.00 = &nbsp;</th>
                </tr>';

    }

    public function genSecondTable(){
        $this->report .= '<tr>
                    <td colspan="6">Homeroom1</td>
                    <td colspan="2" class="setCenter">-</td>
                    <td colspan="2" class="setCenter">-</td>
                    <td class="noBorder" colspan="11"></td>
                </tr>
                <tr>
                    <td colspan="6">Homeroom1</td>
                    <td colspan="2" class="setCenter"></td>
                    <td colspan="2" class="setCenter"></td>
                    <td class="noBorder" colspan="11"></td>
                </tr>
                <tr>
                    <td colspan="6">Homeroom1</td>
                    <td colspan="2" class="setCenter"></td>
                    <td colspan="2" class="setCenter"></td>
                    <td class="noBorder"></td>
                    <td class="noBorder" colspan="10" style="text-align:center;">Classroom signature..........................................</td>
                </tr>
                <tr>
                    <td colspan="6">Homeroom1</td>
                    <td colspan="2" class="setCenter"></td>
                    <td colspan="2" class="setCenter"></td>
                    <td class="noBorder" colspan="11"></td>
                </tr>
                <tr>
                    <td colspan="6">Homeroom1</td>
                    <td colspan="2" class="setCenter"></td>
                    <td colspan="2" class="setCenter"></td>
                    <td class="noBorder" colspan="11"></td>
                </tr>

            </tbody>
            <tbody class="noBorder">
                <tr>
                    <td colspan="21"> &nbsp;</td>
                </tr>
            </tbody>
            <tbody class="setLeft noBorder">
                <tr>
                    <th class="noBorder" colspan="2">Promotion :</th>
                    <th class="setCenter">&nbsp;</th>
                    <td colspan="18" class="setCenter">&nbsp;&nbsp; to be promoted to grade 2 academic year 2017</td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                    <th>&nbsp;</th>
                    <td colspan="18">&nbsp;&nbsp; to be considered</td>
                </tr>
                <tr>
                    <td colspan="21"> &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="21"> &nbsp;</td>
                </tr>
            </tbody>
            <tbody class="noBorder">
                <tr>
                    <td colspan="1"> &nbsp;</td>
                    <td colspan="8" class="setCenter">Asst.Prof.Acharapan Corvanich</td>
                    <td colspan="3"> &nbsp;</td>
                    <td colspan="7">Classroom Teacher</td>
                    <td colspan="2"> &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="1"> &nbsp;</td>
                    <td colspan="8" align="center">IP Chair</td>
                    <td colspan="3"> &nbsp;</td>
                    <td colspan="7">&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;/2017</td>
                    <td colspan="2"> &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="1"> &nbsp;</td>
                    <td colspan="7">&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;/2017</td>
                    <td colspan="3"> &nbsp;</td>
                    <td colspan="7"></td>
                    <td colspan="2"> &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="21"> &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="21"> &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="21"> &nbsp; Remark :</td>
                </tr>
                <tr>
                    <td colspan="21"> &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="21"> &nbsp;</td>
                </tr>
            </tbody>
        </table>
        <button id="print" type="button" onclick="printPage();">Print This Page</button>
    </div>';
    
    }

    public function getReport(){
        return $this->report;
    }

    public function genExample()
    {
        $this->report = '<div id="report">
        <table class="mytable" style="border: hidden;">
            <thead class="noBorder">
                <tr>
                    <th colspan="15" style="text-align:left;">Grade 1 : 2016</th>
                    <!--<th colspan="10">&nbsp;</th>-->
                    <th colspan="6" style="text-align:left;">Classroom Teacher</th>
                </tr>
                <tr>
                    <th colspan="15" style="text-align:left;">Student Name : Amonrada Yodpinit</th>
                    <!--<th colspan="10">&nbsp;</th>-->
                    <td colspan="6" style="text-align:left;">Mr. Ronald Diaz</td>
                </tr>
                <tr>
                    <th colspan="15" style="text-align:left;">Student ID : 2580081060</th>
                    <!--<th colspan="10">&nbsp;</th>-->
                    <td colspan="6" style="text-align:left;">Mr. Tana Ratanadilokchai</td>
                </tr>
                <tr>
                    <th colspan="15" style="text-align:left;">&nbsp;</th>
                    <td colspan="6" style="text-align:left;">Ms. Rungnapa Inta</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </thead>
            <thead>
                <tr>
                    <th rowspan="2" colspan="6">Course</th>
                    <th rowspan="2" colspan="2">Code</th>
                    <th colspan="2">Periods/Week;</th>
                    <th rowspan="2">Credit<br>Hour</th>
                    <th colspan="4" colspan="4">1st Semester Grade</th>
                    <th colspan="4" colspan="4">2st Semester Grade</th>
                    <th rowspan="2" colspan="2">Year<br>Grade</th>
                </tr>
                <tr>
                    <th>In Class</th>
                    <th>Practice</th>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>Sem.<br>Grade</th>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>Sem.<br>Grade</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="6">Thai Language 1</td>
                    <td colspan="2">THA 11103</td>
                    <td>5</td>
                    <td>-</td>
                    <td>5.0</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>-</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>-</td>
                    <td colspan="2">-</td>
                </tr>
                <tr>
                    <td colspan="6">Phycomotor Skills/Physical Education 1</td>
                    <td colspan="2">WORD 11108</td>
                    <td>5</td>
                    <td>-</td>
                    <td>5.0</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>-</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>-</td>
                    <td colspan="2">-</td>
                </tr>
                <tr>
                    <th colspan="10">Total</th>
                    <th>37.0</th>
                    <td colspan="3">1st Semester Grade</td>
                    <td>&nbsp;</td>
                    <td colspan="3">2st Semester Grade</td>
                    <td>&nbsp;</td>
                    <th colspan="2"></th>
                </tr>
                <tr class="noBorder">
                    <td colspan="21"> &nbsp;</td>
                </tr>
                <tr>
                    <th colspan="6">Activity (S/U)</th>
                    <th colspan="2">1st Sem</th>
                    <th colspan="2">2st Sem</th>
                    <th class="noBorder" colspan="4">&nbsp;</th>
                    <th colspan="7">Cumulative GPA/37.0 = 3.99</th>
                </tr>
                <tr>
                    <td colspan="6">Homeroom1</td>
                    <td colspan="2" class="setCenter">11</td>
                    <td colspan="2" class="setCenter">1</td>
                    <td class="noBorder" colspan="11"></td>
                </tr>
                <tr>
                    <td colspan="6">Homeroom1</td>
                    <td colspan="2" class="setCenter"></td>
                    <td colspan="2" class="setCenter"></td>
                    <td class="noBorder" colspan="11"></td>
                </tr>
                <tr>
                    <td colspan="6">Homeroom1</td>
                    <td colspan="2" class="setCenter"></td>
                    <td colspan="2" class="setCenter"></td>
                    <td class="noBorder"></td>
                    <td class="noBorder" colspan="10" style="text-align:center;">Classroom signature..........................................</td>
                </tr>
                <tr>
                    <td colspan="6">Homeroom1</td>
                    <td colspan="2" class="setCenter"></td>
                    <td colspan="2" class="setCenter"></td>
                    <td class="noBorder" colspan="11"></td>
                </tr>
                <tr>
                    <td colspan="6">Homeroom1</td>
                    <td colspan="2" class="setCenter"></td>
                    <td colspan="2" class="setCenter"></td>
                    <td class="noBorder" colspan="11"></td>
                </tr>

            </tbody>
            <tbody class="noBorder">
                <tr>
                    <td colspan="21"> &nbsp;</td>
                </tr>
            </tbody>
            <tbody class="setLeft noBorder">
                <tr>
                    <th class="noBorder" colspan="2">Promotion :</th>
                    <th class="setCenter">&nbsp;</th>
                    <td colspan="18" class="setCenter">&nbsp;&nbsp; to be promoted to grade 2 academic year 2017</td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                    <th>&nbsp;</th>
                    <td colspan="18">&nbsp;&nbsp; to be considered</td>
                </tr>
                <tr>
                    <td colspan="21"> &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="21"> &nbsp;</td>
                </tr>
            </tbody>
            <tbody class="noBorder">
                <tr>
                    <td colspan="1"> &nbsp;</td>
                    <td colspan="8" class="setCenter">Asst.Prof.Acharapan Corvanich</td>
                    <td colspan="3"> &nbsp;</td>
                    <td colspan="7">Classroom Teacher</td>
                    <td colspan="2"> &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="1"> &nbsp;</td>
                    <td colspan="8" align="center">IP Chair</td>
                    <td colspan="3"> &nbsp;</td>
                    <td colspan="7">&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;/2017</td>
                    <td colspan="2"> &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="1"> &nbsp;</td>
                    <td colspan="7">&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;/2017</td>
                    <td colspan="3"> &nbsp;</td>
                    <td colspan="7"></td>
                    <td colspan="2"> &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="21"> &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="21"> &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="21"> &nbsp; Remark :</td>
                </tr>
                <tr>
                    <td colspan="21"> &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="21"> &nbsp;</td>
                </tr>
            </tbody>
        </table>
        <button id="print" type="button" onclick="printPage();">Print This Page</button>
    </div>';
    }
}
