    function login() {
        alert("Hello");
        $.ajax({
            type: "POST",
            url: "controller/php_function_switcher.php",
            data: {
                func: 'get_login',
                username: $('#username').val(),
                password: $('#password').val()
            },
            success: function(data) {
                alert(data);
            },
            error: function(data) {
                alert(data);
            }
        });
    }
    //log out session
    function logOut() {
        $.ajax({
            type: "POST",
            url: 'controller/kill_session.php',
            success: function() {
                window.location.href = "Login.php";
                alert("Loging out");
            }
        });
    }


    function goToClass() {
        window.location.href = "ClassTeacher.php";
    }

    function goToSubject() {
        window.location.href = "SubjectTeacher.php";
    }

    function confirmData() {
        $('#studentTable').hide();
        $('#subjectTable').show();
        // $('.editbtn').attr('disabled', 'disabled');
        alert('Confirm!');
        $('#subject_name').html('Name');
        $('#subject_id').html('ID');
    }

    function confirmInput() {
        var x = document.getElementById("username").value;
        var y = document.getElementById("password").value;
        if (x != "" || y != "") {
            alert("สวัสดีคุณ " + document.getElementById("username").value + " ยินดีต้อนรับสู่ระบบออกเกรดสมุดพกนักเรียน");
            login();
        } else {
            alert("กรอก username หรือ password ผิด!");
        }
    }

    function sendCourse() {
        var year = $('#year').val();
        var semester = $('#semester').val();
        if (year == "--" || semester == "--") {
            if (year == "--") {
                alert("Please select year");
            } else {
                alert("Please select semester");
            }
        } else {
            $.ajax({
                type: "POST",
                url: "controller/php_function_switcher.php",
                data: {
                    func: 'get_coursename',
                    year: $('#year').val(),
                    semester: $('#semester').val(),
                    idTeacher: $('#id_teacher').val()
                },
                success: function(data) {
                    alert("Sent data to Database success.");
                    formatSubject(data);

                },
                error: function(data) {
                    alert("error " + data);
                }
            });
        }
    }

    function getStudent(id) {
        alert("Ready to get student ");
        $.ajax({
            type: "POST",
            url: "controller/php_function_switcher.php",
            data: {
                func: 'get_student_in_course',
                idSubject: id,
                year: $('#year').val(),
                semester: $('#semester').val(),
                idTeacher: $('#id_teacher').val()

            },
            success: function(data) {
                formatStudentName(data);

            }
        });
    }

    function getStudentInClass(id) {
        alert("Get Student by Teacher ID");
        var id2 = $('#id_teacher').val();
        $.ajax({
            type: "POST",
            url: "controller/php_function_switcher.php",
            data: {
                func: 'get_student_in_class',
                idTeacher: $('#id_teacher').val()
            },
            success: function(data) {
                alert('Generate data');
                formatClass(data);
            }

        })
    }



    function formatStudentName(data) {
        $('#studentTable').show();
        $('#subjectTable').hide();
        alert("generate student data");
        var obj = JSON.parse(data);
        $('#subject_name').html(obj[0]['subject_name']);
        $('#subject_id').html(obj[0]['subject_id']);

        var outTab = '<table id="studentTable" align="center" style="border-style: solid;">';
        outTab += '<tr><th>No.</th><th>ID</th><th>firstname</th><th>lastname</th><th>program</th><th>q1</th><th>q2</th><th>q3</th><th>grade</th><th></th><th></th></tr>';
        for (i = 0; i < obj.length; i++) {
            outTab += '<tr>' + '<td>' + (i + 1) + '</td>' +
                '<td>' + obj[i]['student_id'] + '</td>' +
                '<td>' + obj[i]['firstname'] + '</td>' +
                '<td>' + obj[i]['lastname'] + '</td>' +
                '<td contenteditable="false">' + obj[i]['program'] + '</td>' +
                '<td contenteditable="false">' + obj[i]['quarter1'] + '</td>' +
                '<td contenteditable="false">' + obj[i]['quarter2'] + '</td>' +
                '<td contenteditable="false">' + obj[i]['quarter3'] + '</td>' +
                '<td contenteditable="false">' + obj[i]['grade'] + '</td>' +
                '<td><button type="button"  onclick="editQScoreSub(this)">Edit</button></td>' +
                '<td><button type="button"  onclick="CalQScoreSub(this)">Calculate</button></td>'
            '</tr>';
        }
        outTab += '</table>';
        $('#studentTable').html(outTab);
    }

    function formatSubject(data) {
        var obj = JSON.parse(data);
        var outTab = '<table id="studentTable" align="center" style="border-style: solid;">';
        outTab += '<tr><th>NO.</th><th>ID</th><th>Subject Name</th></tr>';
        for (i = 0; i < obj.length; i++) {
            var id = obj[i]['id_subject'] + "";
            outTab += '<tr>' + '<td>' + (i + 1) + '</td>' +
                '<td><a style="cursor:pointer" onclick=getStudent("' + id + '")>' + id + '</td>' +
                '<td>' + obj[i]['name'] + '</td>' +
                '</tr>';
        }
        outTab += '</table>';
        $('#subjectTable').html(outTab);
    }
    //generate student in class room following id teacher
    function formatClass(data) {
        var obj = JSON.parse(data);
        var outTable = '<table id="studentClass" align="center" style="border-style: solid;">';
        outTable += '<tr><th>No.</th><th>ID</th><th>firstname</th><th>lastname</th></tr>';
        for (i = 0; i < obj.length; i++) {
            outTable += '<tr>' + '<td>' + (i + 1) + '</td>' +
                '<td>' + obj[i]['student_id'] + '</td>' +
                '<td>' + obj[i]['firstname'] + '</td>' +
                '<td>' + obj[i]['lastname'] + '</td>' +
                '<td><button type="button"  onclick=editStudent(this);>Edit</button><button type="button" onclick=getReport(this);>Report</button>'
            '</tr>';
        }
        outTable += '</table>';
        $('#studentClass').html(outTable);
    }

    function editStudent(button) {
        var currentTD = $(button).parents('tr').find('td');
        // var tableTD = $('#studentCourse').find('td');        
        // $(currentTD.eq(4)).prop('contenteditable', true);
        // $(currentTD.eq(5)).prop('contenteditable', true);
        // $(currentTD.eq(6)).prop('contenteditable', true);
        $('#studentName').html(currentTD.eq(2).html() + ' ' + currentTD.eq(3).html());
        $('#studentId').html(currentTD.eq(1).html());
        $.ajax({
            type: "POST",
            url: "controller/php_function_switcher.php",
            data: {
                func: 'get_course_in_student',
                idStudent: currentTD.eq(1).html()
            },
            success: function(data) {
                formatCourseStudent(data);
            }
        });
    }


    function formatCourseStudent(data) {
        $('#studentCourse').show();
        $('#studentClass').hide();
        var obj = JSON.parse(data);
        var formatCS = '<table id="studentCourse" align="center" style="border-style: solid;">';
        formatCS += '<tr><th>No.</th><th>id_subject</th><th>subject_name</th><th>credit</th><th>q1</th><th>q2</th><th>q3</th><th>grade</th></tr>';
        for (i = 0; i < obj.length; i++) {
            formatCS += '<tr>' + '<td>' + (i + 1) + '</td>' +
                '<td>' + obj[i]['subject_id'] + '</td>' +
                '<td>' + obj[i]['subject_name'] + '</td>' +
                '<td>' + obj[i]['credit'] + '</td>' +
                '<td>' + obj[i]['quarter1'] + '</td>' +
                '<td>' + obj[i]['quarter2'] + '</td>' +
                '<td> ' + obj[i]['quarter3'] + '</td>' +
                '<td>' + obj[i]['grade'] + '</td>' +
                '<td><button type="button"  onclick="editQScoreClassroom(this)">Edit</button></td>' +
                '<td><button type="button" onclick=CalQScoreClassroom(this) ;>Calculate</button>'
            '</tr>';
        }
        formatCS += '</table>';
        $('#studentCourse').html(formatCS);
    }

    function editQScoreSub(button) {
        var currentTD = $(button).parents('tr').find('td');
        alert("Button was clicked");
        if ($(button).html() == "Edit") {
            $(button).html("Save");
            $(currentTD.eq(5)).prop('contenteditable', true);
            $(currentTD.eq(6)).prop('contenteditable', true);
            $(currentTD.eq(7)).prop('contenteditable', true);
        } else {
            var outData = {};
            var q1 = currentTD.eq(5).html();
            var q2 = currentTD.eq(6).html();
            var q3 = currentTD.eq(7).html();
            outData['subject_id'] = $('#subject_id').html();
            outData['student_id'] = currentTD.eq(1).html();
            outData['quarter1'] = q1;
            outData['quarter2'] = q2;
            outData['quarter3'] = q3;
            outData['year'] = $('#year').val();
            outData['semester'] = $('#semester').val();
            outData['grade'] = currentTD.eq(8).html();
            var json_student = JSON.stringify(outData);
            if (!isEmpty(q1) && !isEmpty(q2) && !isEmpty(q3) && isNumber(q1) && isNumber(q2) && isNumber(q3)) {

                $(button).html("Edit");
                $(currentTD.eq(5)).prop('contenteditable', false);
                $(currentTD.eq(6)).prop('contenteditable', false);
                $(currentTD.eq(7)).prop('contenteditable', false);
                $.ajax({
                    type: "POST",
                    url: "controller/php_function_switcher.php",
                    data: {
                        func: 'update_quarter1',
                        students: json_student
                    },
                    success: function(data) {
                        alert("update success " + data);

                    },
                    error: function(data) {
                        alert("error " + data);
                    }
                });
            } else {
                alert("invalid data");
            }
        }
    }

    function CalQScoreSub(button) {
        var currentTD = $(button).parents('tr').find('td');
        var q1 = currentTD.eq(5).html();
        var q2 = currentTD.eq(6).html();
        var q3 = currentTD.eq(7).html();
        var grade = Number(calculateGrade(q1, q2, q3));
        alert('Calculating');
        grade = round(grade, 2);
        if (grade >= 0) {
            alert('success calculating');
            var outData = {};
            outData['subject_id'] = $('#subject_id').html();
            outData['student_id'] = currentTD.eq(1).html();
            outData['quarter1'] = q1;
            outData['quarter2'] = q2;
            outData['quarter3'] = q3;
            outData['year'] = $('#year').val();
            outData['semester'] = $('#semester').val();
            $(currentTD.eq(8).html(grade));
            outData['grade'] = currentTD.eq(8).html();
            var json_student = JSON.stringify(outData);
            $.ajax({
                type: "POST",
                url: "controller/php_function_switcher.php",
                data: {
                    func: 'update_quarter1',
                    students: json_student
                },
                success: function(data) {
                    alert("update success " + data);

                },
                error: function(data) {
                    alert("error " + data);
                }
            });
        } else {
            alert("invalid " + grade);
        }
    }

    function editQScoreClassroom(button) {
        var currentTD = $(button).parents('tr').find('td');
        alert('Button was clicked');
        if ($(button).html() == "Edit") {

            $(button).html("Save");
            $(currentTD.eq(4)).prop('contenteditable', true);
            $(currentTD.eq(5)).prop('contenteditable', true);
            $(currentTD.eq(6)).prop('contenteditable', true);
        } else {
            var outData = {};
            var q1 = currentTD.eq(4).html();
            var q2 = currentTD.eq(5).html();
            var q3 = currentTD.eq(6).html();
            outData['subject_id'] = currentTD.eq(1).html();
            outData['student_id'] = $('#studentId').html();
            outData['quarter1'] = q1;
            outData['quarter2'] = q2;
            outData['quarter3'] = q3;
            outData['grade'] = currentTD.eq(7).html();
            var json_student = JSON.stringify(outData);
            if (!isEmpty(q1) && !isEmpty(q2) && !isEmpty(q3) && isNumber(q1) && isNumber(q2) && isNumber(q3)) {
                $(button).html("Edit");
                $(currentTD.eq(4)).prop('contenteditable', false);
                $(currentTD.eq(5)).prop('contenteditable', false);
                $(currentTD.eq(6)).prop('contenteditable', false);
                $.ajax({
                    type: "POST",
                    url: "controller/php_function_switcher.php",
                    data: {
                        func: 'update_quarter2',
                        students: json_student
                    },
                    success: function(data) {
                        alert("update success " + data);

                    },
                    error: function(data) {
                        alert("error " + data);
                    }
                });
            } else {
                alert("invalid data");
            }
        }
    }

    function CalQScoreClassroom(button) {
        var currentTD = $(button).parents('tr').find('td');
        var q1 = currentTD.eq(4).html();
        var q2 = currentTD.eq(5).html();
        var q3 = currentTD.eq(6).html();
        var grade = Number(calculateGrade(q1, q2, q3));
        alert('Calculating');
        grade = round(grade, 2);

        if (grade >= 0) {
            alert('Calculating success');
            var outData = {};
            outData['subject_id'] = currentTD.eq(1).html();
            outData['student_id'] = $('#studentId').html();
            outData['quarter1'] = q1;
            outData['quarter2'] = q2;
            outData['quarter3'] = q3;
            $(currentTD.eq(7).html(grade));
            outData['grade'] = currentTD.eq(7).html();
            var json_student = JSON.stringify(outData);
            $.ajax({
                type: "POST",
                url: "controller/php_function_switcher.php",
                data: {
                    func: 'update_quarter2',
                    students: json_student
                },
                success: function(data) {
                    alert("update success " + data);

                },
                error: function(data) {
                    alert("error " + data);
                }
            });
        } else {
            alert("invalid data " + grade);
        }
    }

    function calculateGrade(q1, q2, q3) {

        if (!isEmpty(q1) && !isEmpty(q2) && !isEmpty(q3) && isNumber(q1) && isNumber(q2) && isNumber(q3)) {

            q1 = Number(q1);
            q2 = Number(q2);
            q3 = Number(q3);
            var grade = ((q1 + q2 + q3) / 3);
            grade = grade.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];

            return grade;
        } else {
            alert(false);
            return -1;
        }


    }

    function isEmpty(data) {
        if (data.trim() == "") {
            return true;
        }
        return false;
    }

    function isNumber(data) {
        var x = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "."];
        for (i = 0; i < data.length; i++) {
            if (!$.inArray(data[i], x)) {
                return false;
            }

        }
        return true;
    }

    function round(value, decimal) {
        return Number(Math.round(value + 'e' + decimal) + 'e-' + decimal);
    }

    function getReport(button) {
        var id = $(button).parents('tr').find('td').eq(1).html();
        window.location = 'genReport.html?id="' + id + '"';
    }